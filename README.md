### Проект для хранения webapp кода sentiment-analyser

#### В данном проекте реализовано:
- Хранение кода webapp приложения
- Сборка образа docker
- Пуш образа в docker hub

#### Сборка приложения
` $ mvn install`

#### Запуск приложения
` $ java -jar sentiment-analysis-web-0.0.1-SNAPSHOT.jar --sa.logic.api.url=http://localhost:5000 ` 

#### Сборка контейнера
` $ docker build -f Dockerfile -t $DOCKER_USER_ID/sentiment-analysis-web-app . `

#### Запуск контейнера
``` 
$ docker run -d -p 8080:8080 -e SA_LOGIC_API_URL='http://<container_ip or docker machine ip>:5000' $DOCKER_USER_ID/sentiment-analysis-web-app  
```

#### Публикация контейнера
` $ docker push $DOCKER_USER_ID/sentiment-analysis-web-app `


